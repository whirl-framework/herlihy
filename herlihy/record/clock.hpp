#pragma once

#include <herlihy/history/time.hpp>

namespace herlihy {

struct IClock {
  virtual ~IClock() = default;

  virtual TimePoint Now() = 0;
};

}  // namespace herlihy
