#pragma once

#include <herlihy/history/history.hpp>
#include <herlihy/record/clock.hpp>

namespace herlihy {

class Recorder {
 public:
  using Id = uint64_t;

 public:
  Recorder(IClock& clock)
    : clock_(clock) {
  }

  // Calls

  Id Start(std::string method, Arguments args);

  void AddLabel(Id id, std::string label);

  void Complete(Id id, ReturnValue ret_value);

  void Finalize();

  History GetHistory() {
    return history_;
  }

 private:

 private:
  IClock& clock_;

  Id next_id_ = 0;

  std::map<Id, Call> active_calls_;
  History history_;
};

}  // namespace herlihy
