#include <herlihy/record/recorder.hpp>

namespace herlihy {

uint64_t Recorder::Start(std::string method, Arguments args) {
  Id id = next_id_++;

  Call call;
  call.id = id;
  call.method = std::move(method);
  call.arguments = std::move(args);
  call.start_time = clock_.Now();

  active_calls_.insert_or_assign(id, std::move(call));

  return id;
}

void Recorder::AddLabel(uint64_t id, std::string label) {
  auto it = active_calls_.find(id);

  assert(it != active_calls_.end());
  it->second.labels.push_back(std::move(label));
}

void Recorder::Complete(uint64_t id, ReturnValue ret_value) {
  auto it = active_calls_.find(id);

  assert(it != active_calls_.end());

  Call& call = it->second;
  call.ret_value = ret_value;
  call.end_time = clock_.Now();

  history_.push_back(call);

  active_calls_.erase(it);
}

void Recorder::Finalize() {
  for (auto& [id, call] : active_calls_) {
    history_.push_back(call);
  }
  active_calls_.clear();
}

}  // namespace herlihy
