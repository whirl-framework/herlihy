#pragma once

#include <herlihy/record/clock.hpp>

namespace herlihy {

class ManualClock : public IClock {
  using Ticks = uint64_t;
 public:
  void AdvanceBy(Ticks delta) {
    total_ += delta;
  }

  void AdvanceTo(Ticks future) {
    assert(future >= total_);
    total_ = future;
  }

  TimePoint Now() override {
    return total_;
  }

 private:
  Ticks total_ = 0;
};

}  // namespace herlihy