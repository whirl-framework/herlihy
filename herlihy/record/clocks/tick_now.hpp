#pragma once

#include <herlihy/record/clock.hpp>

namespace herlihy {

// Clocks that move forward by 1 tick for each Now call
IClock& TickNow();

}  // namespace herlihy
