#include <herlihy/record/clocks/tick_now.hpp>

namespace herlihy {

class Clock : public IClock {
 public:
  TimePoint Now() override {
    return ++ticks_;
  }

 private:
  TimePoint ticks_ = 0;
};

IClock& TickNow() {
  static class Clock instance;
  return instance;
}

}  // namespace herlihy
