#pragma once

#include <pickle/serialize.hpp>

#include <string>

namespace herlihy {

struct ReturnValue {
  template <typename TMessage>
  TMessage As() const {
    TMessage arguments;
    pickle::Deserialize(bytes, &arguments);
    return arguments;
  }

  template <typename TMessage>
  static ReturnValue Make(TMessage value) {
    return {pickle::Serialize(value)};
  }

  std::string bytes;
};

}  // namespace herlihy
