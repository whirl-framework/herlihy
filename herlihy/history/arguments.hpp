#pragma once

#include <pickle/serialize.hpp>

#include <string>

namespace herlihy {

struct Arguments {
  template <typename TArgsMessage>
  TArgsMessage As() const {
    TArgsMessage args;
    bool ok = pickle::Deserialize(bytes, &args);
    assert(ok);
    return args;
  }

  template <typename TArgsMessage>
  static Arguments Make(TArgsMessage args) {
    auto bytes = pickle::Serialize(args);
    return {bytes};
  }

  std::string bytes;
};

}  // namespace herlihy
