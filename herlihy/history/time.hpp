#pragma once

#include <cstdint>

namespace herlihy {

using TimePoint = uint64_t;

}  // namespace herlihy
