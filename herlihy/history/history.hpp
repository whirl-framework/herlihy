#pragma once

#include <herlihy/history/call.hpp>

#include <vector>

namespace herlihy {

// Concurrent history
using History = std::vector<Call>;

}  // namespace herlihy
