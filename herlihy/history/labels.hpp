#pragma once

#include <string>
#include <vector>

namespace herlihy {

using CallLabels = std::vector<std::string>;

}  // namespace herlihy
