#pragma once

#include <herlihy/history/time.hpp>
#include <herlihy/history/arguments.hpp>
#include <herlihy/history/value.hpp>
#include <herlihy/history/labels.hpp>

#include <optional>

namespace herlihy {

struct Call {
  size_t id;

  std::string method;
  Arguments arguments;
  TimePoint start_time;

  // Completed
  std::optional<TimePoint> end_time;
  std::optional<ReturnValue> ret_value;

  CallLabels labels;

  bool IsCompleted() const {
    return ret_value.has_value();
  }
};

}  // namespace herlihy
