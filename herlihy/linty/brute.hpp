#pragma once

#include <herlihy/history/history.hpp>

#include <herlihy/linty/real_time_order.hpp>

namespace herlihy::linty {

//////////////////////////////////////////////////////////////////////

struct SearchBudget {
  bool Continue() {
    if (searches_ > 0) {
      --searches_;
      return true;
    } else {
      return false;
    }
  }

  size_t searches_ = 0;
};

//////////////////////////////////////////////////////////////////////

template <typename Model>
class LinChecker {
 public:
  using State = typename Model::State;

 public:
  LinChecker(History history, SearchBudget& budget)
      : calls_(history), count_(history.size()), budget_(budget) {
  }

  bool Check() {
    // Search from initial state
    return Search(Model::InitialState());
  }

 private:
  bool StepInto(size_t i, State next_state);

  bool Search(State curr_state) {
    if (!budget_.Continue()) {
      return true;  // Maybe
    }

    PrintSearchState(curr_state);

    if (count_ == 0) {
      // linear_ - linearization
      return true;
    }

    for (size_t i = 0; i < count_; ++i) {
      if (!NoPrecedingCalls(calls_[i])) {
        continue;
      }

      if (!calls_[i].IsCompleted()) {
        if (StepInto(i, curr_state)) {
          return true;
        }
      }

      auto result = Apply(curr_state, calls_[i]);

      if (result.ok) {
        if (!calls_[i].IsCompleted() || Model::Same(result.ret_value, *(calls_[i].ret_value))) {
          if (StepInto(i, std::move(result.next))) {
            return true;
          }
        }
      }
    }

    return false;
  }

 private:
  typename Model::Result Apply(const State curr, const Call call) {
    return Model::Apply(curr, call.method, call.arguments);
  }

  bool NoPrecedingCalls(const Call& call) const {
    for (size_t i = 0; i < count_; ++i) {
      if (PrecedesInRealTime(calls_[i], call)) {
        return false;
      }
    }
    return true;
  }

  void PrintSearchState(const State& state) {
    using Printer = typename Model::Printer;

    std::cout << "Search state: " << linear_.size() << " calls"
              << std::endl;
    for (size_t i = 0; i < linear_.size(); ++i) {
      std::cout << Printer::Print(linear_[i]) << " -> ";
    }
    std::cout << "State: " << Printer::Print(state) << std::endl;
  }

 private:
  std::vector<Call> calls_;
  size_t count_;

  History linear_;

  SearchBudget budget_;
};

template <typename Model>
bool LinChecker<Model>::StepInto(size_t i, State next_state) {
  linear_.push_back(calls_[i]);
  std::swap(calls_[i], calls_[count_ - 1]);
  count_--;

  bool succeeded = Search(std::move(next_state));

  count_++;
  std::swap(calls_[i], calls_[count_ - 1]);
  linear_.pop_back();

  return succeeded;
}

//////////////////////////////////////////////////////////////////////

template <typename Model>
bool CheckBrute(const History& history, SearchBudget& budget) {
  LinChecker<Model> checker(history, budget);
  return checker.Check();
}

}  // namespace herlihy::linty
