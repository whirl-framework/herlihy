#pragma once

#include <herlihy/history/call.hpp>

namespace herlihy::linty {

// https://jepsen.io/consistency/models/linearizable

inline bool PrecedesInRealTime(const Call& lhs, const Call& rhs) {
  return lhs.IsCompleted() && (*lhs.end_time < rhs.start_time);
}

}  // namespace herlihy::linty
