#pragma once

#include <herlihy/history/history.hpp>

#include <herlihy/linty/brute.hpp>

namespace herlihy::linty {

template <typename Model>
History RemoveNotCompletedReads(const History& history) {
  History result;
  for (const auto& call : history) {
    if (!call.IsCompleted() && !Model::IsMutation(call)) {
      // Skip
    } else {
      result.push_back(call);
    }
  }
  return result;
}

template <typename Model>
bool Check(History history) {
  // Cleanup
  history = RemoveNotCompletedReads<Model>(history);
  // Decompose to independent histories
  auto sub_histories = Model::Decompose(history);
  // Check

  SearchBudget budget{100'500};

  for (const auto& sub_history : sub_histories) {
    if (!CheckBrute<Model>(sub_history, budget)) {
      return false;
    }
  }
  return true;
}

}  // namespace herlihy::linty
