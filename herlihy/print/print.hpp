#pragma once

#include <herlihy/history/history.hpp>

#include <ostream>
#include <sstream>
#include <string>
#include <set>
#include <map>

namespace herlihy {

//////////////////////////////////////////////////////////////////////

namespace detail {

std::string PrintLabels(const CallLabels &labels);
std::string MakeSpace(TimePoint start_ts);
std::string MakeCallSegment(TimePoint start, TimePoint end, bool completed);

}  // namespace detail

//////////////////////////////////////////////////////////////////////

template <typename Printer>
void Print(const History& history, std::ostream& out) {
  // Collect time points

  std::set<TimePoint> tps;
  for (const auto& call : history) {
    tps.insert(call.start_time);
    if (call.IsCompleted()) {
      tps.insert(*call.end_time);
    }
  }

  // Compact time points

  std::map<TimePoint, TimePoint> compact_tps;
  size_t ctp = 0;
  for (auto tp : tps) {
    ++ctp;
    compact_tps[tp] = ctp;
  }
  size_t max_tp = ctp + 1;

  for (size_t i = 0; i < history.size(); ++i) {
    const auto& call = history[i];

    TimePoint start_ts = compact_tps[call.start_time];

    TimePoint end_ts;
    if (call.IsCompleted()) {
      end_ts = compact_tps[*call.end_time];
    } else {
      end_ts = max_tp;
    }

    out << detail::MakeSpace(start_ts) << (i + 1) << ". " << Printer::Print(call)
        << "\t" << detail::PrintLabels(call.labels) << std::endl;

    out << detail::MakeSpace(start_ts)
        << detail::MakeCallSegment(start_ts, end_ts, call.IsCompleted()) << std::endl;
  }
}

}  // namespace herlihy
