#include <herlihy/print/print.hpp>

namespace herlihy {

static const size_t kUnitLength = 5;

namespace detail {

std::string PrintLabels(const CallLabels &labels) {
  if (labels.empty()) {
    return "";
  }

  std::stringstream out;
  out << "{";
  for (size_t i = 0; i < labels.size(); ++i) {
    out << labels[i];
    if (i + 1 < labels.size()) {
      out << ", ";
    }
  }
  out << "}";
  return out.str();
}

std::string MakeSpace(TimePoint start_ts) {
  return std::string(start_ts * kUnitLength, ' ');
};

std::string MakeCallSegment(TimePoint start, TimePoint end,
                            bool completed) {
  std::string call((end - start) * kUnitLength, '-');
  call[0] = '[';
  if (completed) {
    call[call.length() - 1] = ']';
  } else {
    call[call.length() - 1] = '~';
  }
  return call;
}

}  // namespace detail

}  // namespace herlihy