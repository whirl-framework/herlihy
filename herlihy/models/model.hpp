#pragma once

#include <herlihy/history/call.hpp>

#include <string>
#include <type_traits>

namespace herlihy {

template <typename M>
concept Model = requires (M model, typename M::State state, std::string method, Arguments args) {
  typename M::State;
  typename M::Result;

  { model.Apply(state, method, args) } -> std::same_as<typename M::Result>;
};

}  // namespace herlihy
