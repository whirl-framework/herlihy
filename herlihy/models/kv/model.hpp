#pragma once

#include <herlihy/models/model.hpp>
#include <herlihy/history/history.hpp>

#include <herlihy/models/kv/state.hpp>
#include <herlihy/models/kv/printer.hpp>

namespace herlihy::models::kv {

class Model {
 public:
  using State = kv::State;

  using Printer = kv::Printer;

  struct Result {
    bool ok;
    State next;
    ReturnValue ret_value;
  };

  static bool IsMutation(const Call& call) {
    return call.method == "Set" || call.method == "Cas";
  }

  static bool Same(const ReturnValue& lhs, const ReturnValue& rhs) {
    return lhs.bytes == rhs.bytes;
  }

  // TODO: Decompose by key
  static std::vector<History> Decompose(const History& history) {
    return {history};
  }

  // Transitions

  static State InitialState() {
    return State{};
  }

  static Result Apply(const State& curr,
                      const std::string& method,
                      const Arguments& args);

 private:
  static Result ApplySet(const State& curr, const Arguments& args);
  static Result ApplyGet(const State& curr, const Arguments& args);
  static Result ApplyCas(const State& curr, const Arguments& args);
};

}  // namespace herlihy::models::kv
