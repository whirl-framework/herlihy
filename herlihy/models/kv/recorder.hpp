#pragma once

#include <herlihy/models/kv/state.hpp>

#include <herlihy/record/recorder.hpp>

namespace herlihy::models::kv {

class Recorder {
  using Impl = herlihy::Recorder;
 public:
  struct GetRecord {
    GetRecord(Impl& recorder, Key key);

    void Label(std::string label);
    void Complete(Value value);
    void Ignore() {};

   private:
    Impl& recorder_;
    Impl::Id id_;
  };

  struct SetRecord {
    SetRecord(Impl& recorder, Key key, Value value);

    void Label(std::string label);
    void Complete();
    void Ignore() {};

   private:
    Impl& recorder_;
    Impl::Id id_;
  };

  struct CasRecord {
    CasRecord(Impl& recorder, Key key, Value e, Value d);

    void Label(std::string label);
    void Complete(Value old_value);
    void Ignore() {};

   private:
    Impl& recorder_;
    Impl::Id id_;
  };

 public:
  Recorder(Impl& impl)
    : impl_(impl) {
  }

  GetRecord Get(Key key) {
    return {impl_, key};
  }

  SetRecord Set(Key key, Value value) {
    return {impl_, key, value};
  }

  CasRecord Cas(Key key, Value e, Value d) {
    return {impl_, key, e, d};
  }

 private:
  herlihy::Recorder& impl_;
};

}  // namespace herlihy::models::kv
