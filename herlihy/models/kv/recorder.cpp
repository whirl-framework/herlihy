#include <herlihy/models/kv/recorder.hpp>

#include <herlihy/models/kv/proto/cpp/ops.pb.h>

namespace ops = proto::herlihy::models::kv;

namespace herlihy::models::kv {

//////////////////////////////////////////////////////////////////////

Recorder::GetRecord::GetRecord(Impl& impl, Key key)
  : recorder_(impl) {

  ops::GetArgs args;
  args.set_key(key);

  id_ = recorder_.Start("Get", Arguments::Make(args));
}

void Recorder::GetRecord::Label(std::string label) {
  recorder_.AddLabel(id_, label);
}

void Recorder::GetRecord::Complete(Value value) {
  ops::Value ret;
  ret.set_data(value);

  recorder_.Complete(id_, ReturnValue::Make(ret));
}

//////////////////////////////////////////////////////////////////////

Recorder::SetRecord::SetRecord(Impl& impl, Key key, Value value)
    : recorder_(impl) {

  ops::SetArgs args;
  args.set_key(key);
  args.set_value(value);

  id_ = recorder_.Start("Set", Arguments::Make(args));
}

void Recorder::SetRecord::Label(std::string label) {
  recorder_.AddLabel(id_, label);
}

void Recorder::SetRecord::Complete() {
  ops::Ack ret;
  recorder_.Complete(id_, ReturnValue::Make(ret));
}

//////////////////////////////////////////////////////////////////////

Recorder::CasRecord::CasRecord(Impl& impl, Key key, Value e, Value d)
    : recorder_(impl) {

  ops::CasArgs args;
  args.set_key(key);
  args.set_expected_value(e);
  args.set_desired_value(d);

  id_ = recorder_.Start("Cas", Arguments::Make(args));
}

void Recorder::CasRecord::Label(std::string label) {
  recorder_.AddLabel(id_, label);
}

void Recorder::CasRecord::Complete(Value old_value) {
  ops::Value ret;
  ret.set_data(old_value);

  recorder_.Complete(id_, ReturnValue::Make(ret));
}

}  // namespace herlihy::models::kv
