#pragma once

#include <herlihy/history/call.hpp>
#include <herlihy/models/kv/state.hpp>

#include <ostream>

namespace herlihy::models::kv {

struct Printer {
  static std::string Print(const Call& call);
  static std::string Print(const State& state);

 private:
  static void PrintGet(const Call& call, std::ostream& out);
  static void PrintSet(const Call& call, std::ostream& out);
  static void PrintCas(const Call& call, std::ostream& out);
};

}  // namespace herlihy::models::kv
