#include <herlihy/models/kv/model.hpp>

#include <herlihy/models/kv/proto/cpp/ops.pb.h>

namespace ops = proto::herlihy::models::kv;

namespace herlihy::models::kv {

Model::Result Model::Apply(const State& curr,
                               const std::string& method,
                               const Arguments& args) {
  if (method == "Set") {
    return ApplySet(curr, args);
  } else if (method == "Get") {
    return ApplyGet(curr, args);
  } else if (method == "Cas") {
    return ApplyCas(curr, args);
  }

  std::abort();  // Method not supported
}

Model::Result Model::ApplySet(const State& curr, const Arguments& args) {
  State next = curr;

  auto set = args.As<ops::SetArgs>();

  next.Set(set.key(), set.value());

  ops::Ack ack;
  return {true, std::move(next), ReturnValue::Make(ack)};
}

Model::Result Model::ApplyGet(const State& curr, const Arguments& args) {
  State next = curr;

  auto get = args.As<ops::GetArgs>();

  ops::Value ret;
  ret.set_data(curr.Get(get.key()));

  return {true, std::move(next), ReturnValue::Make(ret)};
}

Model::Result Model::ApplyCas(const State& curr, const Arguments& args) {
  State next = curr;

  auto cas = args.As<ops::CasArgs>();

  Value old_value = next.Cas(cas.key(),
                             cas.expected_value(),
                             cas.desired_value());

  ops::Value ret;
  ret.set_data(old_value);

  return {true, next, ReturnValue::Make(ret)};
}


}  // namespace herlihy::models::kv
