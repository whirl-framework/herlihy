#include <herlihy/models/kv/printer.hpp>

#include <herlihy/models/kv/proto/cpp/ops.pb.h>

#include <sstream>

namespace ops = proto::herlihy::models::kv;

namespace herlihy::models::kv {

//////////////////////////////////////////////////////////////////////

std::string Printer::Print(const Call& call) {
  std::stringstream out;

  if (call.method == "Set") {
    PrintSet(call, out);
  } else if (call.method == "Get") {
    PrintGet(call, out);
  } else if (call.method == "Cas") {
    PrintCas(call, out);
  }

  return out.str();
}

void Printer::PrintSet(const Call& call, std::ostream& out) {
  // Name
  out << call.method;

  // Arguments
  auto set = call.arguments.As<ops::SetArgs>();
  out << "(" << set.key() << ", " << set.value() << ")";

  // Return value
  if (call.IsCompleted()) {
    // out << ": void";
  } else {
    out << "?";
  }
}

void Printer::PrintGet(const Call& call, std::ostream& out) {
  out << call.method;

  // Arguments
  auto get = call.arguments.As<ops::GetArgs>();
  out << "(" << get.key() << ")";

  // Return value
  if (call.IsCompleted()) {
    auto value = call.ret_value->As<ops::Value>();
    out << ": " << value.data();
  } else {
    out << "?";
  }
}

void Printer::PrintCas(const Call& call, std::ostream& out) {
  out << call.method;

  // Arguments
  auto cas = call.arguments.As<ops::CasArgs>();
  out << "(" << cas.key() << ", " << cas.expected_value() << ", " << cas.desired_value() << ")";

  // Return value
  if (call.IsCompleted()) {
    auto old_value = call.ret_value->As<ops::Value>();
    out << ": " << old_value.data();
  } else {
    out << "?";
  }
}

//////////////////////////////////////////////////////////////////////

std::string Printer::Print(const State& state) {
  auto entries = state.GetEntries();

  std::stringstream out;
  out << "{";

  bool first = true;

  for (const auto& [k, v] : entries) {
    if (!first) {
      out << ", ";
      first = false;
    }
    out << k << " -> " << v;
  }

  out << "}";

  return out.str();
}

}  // namespace herlihy::models::kv
