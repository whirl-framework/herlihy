#pragma once

#include <string>
#include <map>

namespace herlihy::models::kv {

//////////////////////////////////////////////////////////////////////

using Key = std::string;
using Value = std::string;

//////////////////////////////////////////////////////////////////////

class State {
  using Entries = std::map<Key, Value>;

 public:
  void Set(const Key& key, Value value) {
    entries_.insert_or_assign(key, std::move(value));
  }

  std::string Get(const Key& key) const {
    auto it = entries_.find(key);
    if (it != entries_.end()) {
      return it->second;
    } else {
      return KeyNotFound();
    }
  }

  std::string Cas(const Key& key, const Value& expected, Value desired) {
    Value curr = Get(key);
    if (curr == expected) {
      Set(key, desired);
    }
    return curr;
  }

  const Entries& GetEntries() const {
    return entries_;
  }

 private:
  static std::string KeyNotFound() {
    return "/";
  }

 private:
  Entries entries_;
};

}  // namespace herlihy::models::kv
