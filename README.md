# Herlihy

[Linearizability](https://jepsen.io/consistency/models/linearizable) checker

Named after [Maurice Herlihy](https://en.wikipedia.org/wiki/Maurice_Herlihy), author of [Linearizability: A Correctness Condition for
Concurrent Objects](https://cs.brown.edu/~mph/HerlihyW90/p463-herlihy.pdf)

---

```
     1. Set(key1, value1)	
     [-------------]
               2. Get(key1): value1	
               [--------]
          3. Set(key1, value2)	
          [------------------]
```

---

[Examples](example/main.cpp)
