#include <herlihy/models/kv/recorder.hpp>
#include <herlihy/models/kv/printer.hpp>
#include <herlihy/models/kv/model.hpp>

#include <herlihy/record/recorder.hpp>
#include <herlihy/record/clocks/tick_now.hpp>
#include <herlihy/record/clocks/manual.hpp>

#include <herlihy/print/print.hpp>

#include <herlihy/linty/check.hpp>

#include <iostream>

//////////////////////////////////////////////////////////////////////

void Example1() {
  std::cout << "Example #1" << std::endl;

  herlihy::Recorder recorder(herlihy::TickNow());

  {
    // Record history
    herlihy::models::kv::Recorder kv(recorder);

    auto set1 = kv.Set("key1", "value1");
    auto set2 = kv.Set("key1", "value2");

    auto get = kv.Get("key1");

    set1.Complete();

    get.Complete("value1");

    set2.Complete();
  }

  auto history = recorder.GetHistory();

  {
    // Print history
    herlihy::Print<herlihy::models::kv::Printer>(history, std::cout);
  }

  {
    // Check linearizability

    bool good = herlihy::linty::Check<herlihy::models::kv::Model>(history);
    std::cout << "Linearizable: " << (good ? "Yes" : "No") << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void Example2() {
  std::cout << "Example #2" << std::endl;

  herlihy::Recorder recorder(herlihy::TickNow());

  {
    // Record history
    herlihy::models::kv::Recorder kv(recorder);

    auto set1 = kv.Set("key1", "value1");
    set1.Complete();

    auto set2 = kv.Set("key1", "value2");
    set2.Complete();

    auto get = kv.Get("key1");
    get.Complete("value1");
  }

  auto history = recorder.GetHistory();

  {
    // Print history
    herlihy::Print<herlihy::models::kv::Printer>(history, std::cout);
  }

  {
    // Check linearizability

    bool good = herlihy::linty::Check<herlihy::models::kv::Model>(history);
    std::cout << "Linearizable: " << (good ? "Yes" : "No") << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void Example3() {
  std::cout << "Example #3" << std::endl;

  herlihy::ManualClock clock;
  herlihy::Recorder recorder(clock);

  {
    // Record history
    herlihy::models::kv::Recorder kv(recorder);

    clock.AdvanceBy(1);

    {
      auto set = kv.Set("key1", "value1");
      set.Label("tid-1");
      set.Ignore();
    }

    clock.AdvanceBy(10);

    {
      auto get1 = kv.Get("key1");
      get1.Label("tid-2");

      clock.AdvanceBy(7);

      get1.Complete("value1");
    }

    clock.AdvanceTo(100);

    {
      auto get2 = kv.Get("key1");
      get2.Label("tid-3");

      clock.AdvanceBy(10);

      get2.Complete("/");
    }

    recorder.Finalize();
  }

  auto history = recorder.GetHistory();

  {
    // Print history
    herlihy::Print<herlihy::models::kv::Printer>(history, std::cout);
  }

  {
    // Check linearizability

    bool good = herlihy::linty::Check<herlihy::models::kv::Model>(history);
    std::cout << "Linearizable: " << (good ? "Yes" : "No") << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  Example1();
  Example2();
  Example3();

  return 0;
}
